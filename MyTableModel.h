#pragma once

#include <MyTableItem.h>

#include <QAbstractTableModel>

class MyTableModel : public QAbstractTableModel {
  Q_OBJECT
public:
  MyTableModel(QObject* parent=nullptr);

public: // QAbstractItemModel interface
  int rowCount(const QModelIndex& parent) const override;
  int columnCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex& index, int role) const override;

  QHash<int, QByteArray> roleNames() const override {
      return {{Qt::DisplayRole, "display"}};
  }

public:
  void initModel(const QList<MyTableItem> &rows);

private:
  QList<MyTableItem>    mRows;
};
