#pragma once

#include "MyTableModel.h"

#include <QMainWindow>
#include <QProperty>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

#include "ui_MainWindow.h"

class MainWindow : public QMainWindow, Ui::MainWindow {
  Q_OBJECT
  Q_PROPERTY(const QAbstractItemModel* model READ model NOTIFY modelChanged)
  Q_PROPERTY(bool animateTable READ animateTable WRITE setAnimateTable NOTIFY animateTableChanged)

public:
  MainWindow(QWidget* parent = nullptr);

  auto model() const { return mActiveModel; }
  Q_SIGNAL void modelChanged();

  auto animateTable() const { return btnAnimateTable->isChecked(); }
  inline void setAnimateTable(bool value) { btnAnimateTable->setChecked(value); }
  Q_SIGNAL void animateTableChanged() const;

private:
  QAbstractItemModel* mActiveModel = nullptr;
  QStandardItemModel mStandardModel;
  MyTableModel mTableModel;
  QSortFilterProxyModel mSortModel;

  void createStandardItems(int count);
  void createMyItems(int count);
};
