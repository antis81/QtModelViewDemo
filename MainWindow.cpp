#include "MainWindow.h"

#include <MyTableItem.h>
#include <MyTableModel.h>

#include <QQmlContext>
#include <QStandardItem>
#include <QElapsedTimer>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
{
    setupUi(this);

    connect(btnCreateItems, &QAbstractButton::clicked, this, [this]{
        auto model = view->model();
        auto count = spinBox->value();
        QElapsedTimer stopWatch;

        stopWatch.start();
        if (model == &mStandardModel) {
            qDebug("Creating %d QStandardItem's ...", count);
            createStandardItems(count);
        } else if (model == &mSortModel || model == &mTableModel) {
            qDebug("Creating %d MyItem's ...", count);
            createMyItems(count);
        }
        qDebug("Created %d items in %lld ms", model->rowCount(), stopWatch.elapsed());
    });

    connect(btnAnimateTable, &QCheckBox::stateChanged, [this](auto state){
        Q_EMIT animateTableChanged();
    });

    auto setModel = [this](auto index){
      if (index == 0) {
        mActiveModel = &mStandardModel;
      } else if (index == 1) {
        if (btnSortable->isChecked()) {
          mActiveModel = &mSortModel;
        } else {
          mActiveModel = &mTableModel;
        }
      } else {
        mActiveModel = nullptr;
      }

      view->setModel(mActiveModel);
      Q_EMIT modelChanged();
    };
    connect(comboBox, &QComboBox::currentIndexChanged, this, setModel);
    connect(btnSortable, &QCheckBox::stateChanged, this, [setModel, this](auto state){
      view->setSortingEnabled(state == Qt::CheckState::Checked);
      setModel(comboBox->currentIndex());
    });

    // set initial state
    mSortModel.setSourceModel(&mTableModel);
    view->setSortingEnabled(btnSortable->isChecked());
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    setModel(comboBox->currentIndex());

    //QProperty<QSortFilterProxyModel*> model = &mSortModel;
    quickWidget->rootContext()->setContextProperty(QStringLiteral("demo"), this);
    quickWidget->setSource(QUrl::fromLocalFile(QStringLiteral(":/qml/MyTableView.qml")));
}

void MainWindow::createStandardItems(int count) {
    auto listOne = QStringLiteral("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z").split(QLatin1Char(' '));
    auto digits = QString::number(count).size();

    mStandardModel.setRowCount(count);
    mStandardModel.setColumnCount(2);
    view->setUpdatesEnabled(false);

    for (int row = 0; row < count; row++) {
        QList<QStandardItem*> columns;
        columns << new QStandardItem(QString::fromUtf8("#%1").arg(row, digits, 10, QLatin1Char('0')))
                << new QStandardItem(listOne[row % listOne.size()]);

        for ( int col = 0; col < columns.size(); col++) {
            mStandardModel.setItem(row, col, columns[col]);
        }
    }

    view->setUpdatesEnabled(true);
    view->update();
}

void MainWindow::createMyItems(int count) {
    auto listOne = QStringLiteral("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z").split(QLatin1Char(' '));
    auto digits = QString::number(count).size();

    QList<MyTableItem> rows;
    for (int row = 0; row < count; row++) {
        MyTableItem newRow;
        newRow.setData(0, QStringLiteral("#%1").arg(row, digits, 10, QLatin1Char('0')));
        newRow.setData(1, listOne[row % listOne.size()]);
        rows << newRow;
    }

    mTableModel.initModel(rows);
}
