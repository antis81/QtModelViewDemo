# Example Application Using Qt's Table Model/View Concept

## QAbstractTableModel vs. QStandardItemModel
The example currently implements a table view that can switch between QAbstractTableModel and QStandardItemModel.
Useful as a "test bed" for testing/improving your model performance.

No license: Have fun!

## Build Requirements

* Qt6 (including CMake and a C++17 compiler)
