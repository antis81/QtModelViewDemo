#pragma once

#include <QVariant>
#include <QList>

class MyTableItem
{
public:
    typedef QMap<int, QVariant> Data;

public:
    MyTableItem() = default;
    virtual ~MyTableItem() = default;

    inline auto columnCount() const { return mColumns.size(); }

    inline QVariant data(int column, int role) const {
        return mColumns[column][role];
    }
    inline void setData(int column, const QVariant& value, int role = Qt::DisplayRole) {
      Q_ASSERT(column >= 0);
      while (columnCount() <= column) {
          mColumns << Data();
      }
      mColumns[column][role] = value;
    }

private:
    QList<Data> mColumns;
};

