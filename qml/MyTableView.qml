import QtQuick
import QtQuick.Controls

Rectangle {
  id: root
  width: 400
  height: 400
  clip:true
  border {color: 'black'; width: 1 }
  color: '#123'

  TableView {
    id: my_table
    topMargin: h_header.height
    leftMargin: v_header.width
    anchors.fill: parent
    clip: true
    model: demo && demo.model
    onModelChanged: {
      console.log("Table model has changed.")
      console.log("Demo Object ->", demo)
      if (demo) {
        console.log("Demo Model property ->", demo.model)
      }
    }
    rowSpacing: 1
    columnSpacing: 1
    selectionModel: ItemSelectionModel { model: my_table.model }
    delegate: Rectangle {
      implicitWidth: 50
      implicitHeight: 35
      TableView.onPooled: if (rotationAnimation.running) { rotationAnimation.pause(); }
      TableView.onReused: if (rotationAnimation.running) { rotationAnimation.resume(); }
      color: selected ? 'lightblue' : 'white'

      required property bool selected

      Text {
        id: cell_content
        anchors.centerIn: parent
        text: display

        // inspired by Qt example -> qthelp://org.qt-project.qtquick.630/qtquick/qml-qtquick-tableview.html
        RotationAnimation {
          id: rotationAnimation
          target: cell_content
          duration: (Math.random() * 2000) + 400
          from: 0
          to: 359
          running: demo ? demo.animateTable : false
          loops: Animation.Infinite
        }
      }
    }
    ScrollBar.vertical: ScrollBar {}
    ScrollBar.horizontal: ScrollBar {}
  }
  VerticalHeaderView {
    id: v_header
    syncView: my_table
    clip: true
  }
  HorizontalHeaderView {
    id: h_header
    syncView: my_table
  }
  SelectionRectangle {
    target: my_table
    selectionMode: SelectionRectangle.Drag
  }
}
